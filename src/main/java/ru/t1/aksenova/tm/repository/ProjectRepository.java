package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.IProjectRepository;
import ru.t1.aksenova.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

}
