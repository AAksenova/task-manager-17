package ru.t1.aksenova.tm.repository;

import ru.t1.aksenova.tm.api.repository.ITaskRepository;
import ru.t1.aksenova.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> findTasks = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (projectId.equals(task.getProjectId())) findTasks.add(task);
        }
        if (findTasks.size() <= 0) return Collections.emptyList();
        return findTasks;
    }

}
