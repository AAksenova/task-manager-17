package ru.t1.aksenova.tm.command.user;

import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "view-user-profile";

    public static final String DESCRIPTION = "View profile of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("USER ID: " + user.getId());
        System.out.println("USER LOGIN: " + user.getLogin());
        System.out.println("USER E-MAIL: " + user.getEmail());
        System.out.println("USER FIRST NAME: " + user.getFirstName());
        System.out.println("USER LAST NAME: " + user.getLastName());
        System.out.println("USER MIDDLE NAME: " + user.getMiddleName());
        System.out.println("USER ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
