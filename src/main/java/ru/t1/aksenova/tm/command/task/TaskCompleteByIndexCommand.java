package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-completes-by-index";

    public static final String DESCRIPTION = "Complete task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;

        getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
    }

}
