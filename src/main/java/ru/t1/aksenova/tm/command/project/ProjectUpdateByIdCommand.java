package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-id";

    public static final String DESCRIPTION = "Update project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        getProjectService().updateById(userId, id, name, description);
    }

}
