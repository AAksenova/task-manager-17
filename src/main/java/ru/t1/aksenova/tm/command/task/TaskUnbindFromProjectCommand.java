package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-unbind-to-project";

    public static final String DESCRIPTION = "Unlink task from project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();

        getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
    }

}
