package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();

        getProjectService().create(userId, name, description);
    }

}
