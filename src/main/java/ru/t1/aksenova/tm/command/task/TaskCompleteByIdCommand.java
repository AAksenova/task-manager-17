package ru.t1.aksenova.tm.command.task;

import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-complete-by-id";

    public static final String DESCRIPTION = "Complete task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();

        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

}
