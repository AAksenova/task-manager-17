package ru.t1.aksenova.tm.command.project;

import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-index";

    public static final String DESCRIPTION = "Remove project by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("[ENTER PROJECT INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;

        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeTaskToProject(userId, project.getId());
    }

}
