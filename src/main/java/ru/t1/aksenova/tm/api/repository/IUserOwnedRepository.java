package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    void clear(String userId);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}
